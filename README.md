Role Name
=========

Role to add/update users with passwords and give permissions to specifique folders

Requirements
------------

None

Role Variables
--------------

user_accounts:
  - username:  name of user
    password: password
    group:  group
    folders:  collection of folders
      - { path:  "path/to/folder", mode: '755' }
      ...
  ...

Dependencies
------------

None

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: ansible-role-userconfig, user: system }

License
-------

BSD

Author Information
------------------

Koraichi Mossaab
