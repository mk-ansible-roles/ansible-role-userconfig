import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']
).get_hosts('all')


def test_hosts_file(host):
    f = host.file('/etc/hosts')

    assert f.exists
    user_devops = host.user('devops')
    assert user_devops.exists
    assert user_devops.home == '/home/devopsddd'
    assert user_devops.group == 'devops'

    user_admin = host.user('admin')
    assert user_admin.exists
    assert user_admin.home == '/home/admin'
    assert user_admin.group == 'admin'



